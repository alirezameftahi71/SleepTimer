﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
    Author: Ali Reza M
    Create Date: Oct 31'16
    Last Update: May 5'17
    Discription: A program to automaticaly shutdown, restart and put the computer to sleep.
*/

namespace TimerSleep
{
    public partial class Form1 : Form
    {
        [DllImport("PowrProf.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);
        public int secs = 0, mins = 0, hours = 0, total = 0;
        public Form1()
        {
            InitializeComponent();
        }
        private void func1()
        {
            if (comboBox1.SelectedIndex == 2)
            {
                Process.Start("shutdown", "/s /t 0");
                Close();
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                Process.Start("shutdown", "/r /t 0");
                Close();
            }
            else if (comboBox1.SelectedIndex == 0)
            {
                SetSuspendState(false, true, true);
                Close();
            }
            else
            {
                MessageBox.Show("Select one of the options from the combo box above.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void func2()
        {
            secs = Convert.ToInt32(textBox1.Text);
            mins = Convert.ToInt32(textBox2.Text);
            hours = Convert.ToInt32(textBox3.Text);
            total = (hours * 3600) + (mins * 60) + secs;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                textBox1.Visible = false;
                textBox2.Visible = false;
                textBox3.Visible = false;
                label2.Visible = false;
                label3.Visible = false;
                label4.Visible = false;
                label6.Visible = true;
                label5.Visible = true;
                total--;
                label6.Text = total.ToString();
                if (total == 0)
                {
                    timer1.Stop();
                    comboBox1.Enabled = true;
                    func1();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Something happened :(", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                func2();
                timer1.Interval = 1000;
                timer1.Start();
                comboBox1.Enabled = false;
                button1.Visible = false;
                button2.Visible = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Select a valid duration for the timer in secends.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            comboBox1.Enabled = true;
            button2.Visible = false;
            button1.Visible = true;
            label6.Visible = false;
            label5.Visible = false;
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            button2.Visible = false;
            button1.Visible = true;
        }
    }
}
